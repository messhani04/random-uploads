var LocalStorageAdvanced = function(_storageName, _defaultValues) {
	var storageName;
	var defaultValues;

	var init = function(_storageName, _defaultValues) {
		if (!_storageName) { throw "LocalStorageAdvanced: no 'name' defined"; }
		storageName = _storageName;
		defaultValues = _defaultValues || {};

		if (!JSON.parse(localStorage.getItem(storageName))) {
			this.Save(defaultValues);
		}
	}.bind(this);

	this.Get = function() { return JSON.parse(localStorage.getItem(storageName)); };
	this.Save = function(options) { localStorage.setItem(storageName, JSON.stringify(options)); };

	this.GetItem = function(name, defaultValue) {
		if (this.Get().hasOwnProperty(name)) {
			return this.Get()[name];
		}
		return defaultValue || null;
	};

	this.SetItem = function(name, value, isNewItem) { 
		var options = this.Get();
		if (options.hasOwnProperty(name) || isNewItem) {
			options[name] = value;
			this.Save(options);
		}
	};

	this.RemoveItem = function(name) {
		var options = this.Get();
		if (options.hasOwnProperty(name)) {
			delete options[name];
		}
		this.Save(options);
	};

	this.SetToDefault = function() { this.Save(defaultValues); };

	init(_storageName, _defaultValues);
};