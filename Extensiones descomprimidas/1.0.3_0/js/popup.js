var bg = chrome.extension.getBackgroundPage(), t = chrome.tabs, cr = chrome.runtime;
var Options = bg.Options;

t.query({currentWindow: true, active: true}, function(tabs) {
  var tab = tabs[0], url = tab.url;

  var active = Options.GetItem("active") || false;
  $("#btn-on-off").text(active ? 'disable' : 'enable');

  $("#btn-options").on("click", function() {
    t.create({url: "options.html"});
    window.close();
  });

  $("#btn-on-off").on("click", function() {
    if (Options.GetItem("password_active")) {
      commonCheckPasswordAndShow($(".wot-popup-body"), function() {
        bg.switchOnOff();
        $("#btn-on-off").text(Options.GetItem("active") ? 'disable' : 'enable');
        bg.reloadTab(tab);
      });
    } else {
      bg.switchOnOff();
      $("#btn-on-off").text(Options.GetItem("active") ? 'disable' : 'enable');
      bg.reloadTab(tab);
    }
  });

  if (url.indexOf("http") !== 0) {
    $(".wot-popup-body").find("*").css("cursor", "not-allowed");
    $("#see").hide();
  } else {
    $("#txt-domain").text(bg.Utils.extractDomain(url).domain);
    $("#see").on("click", function() {
      t.create({url: "https://www.mywot.com/en/scorecard/" + bg.Utils.extractDomain(url).domain});
      window.close();
    });

    $("#btn-block").on("click", function() {
      if (Options.GetItem("password_active")) {
        commonCheckPasswordAndShow($(".wot-popup-body"), function() {
          bg.blockSite(tab);
          window.close();
        });
      } else {
        bg.blockSite(tab);
        window.close();
      }
    });

    $("#btn-trust").on("click", function() {
      if (Options.GetItem("password_active")) {
        commonCheckPasswordAndShow($(".wot-popup-body"), function() {
          bg.trustSite(tab);
          window.close();
        });
      } else {
        bg.trustSite(tab);
        window.close();
      }
    });
  }

  $('#btn-hide').click(function(e) {
    var that = $(this), enable = that.text().indexOf("S") === 0;
    Options.SetItem("showIcon", enable);
    chrome.runtime.sendMessage({message: "toggleFromPopupIcon", showIcon: enable});
    window.close();
  });
});


