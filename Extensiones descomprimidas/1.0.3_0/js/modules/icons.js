var Icons = (function () {
    var ICONBAR_DEFAULT_19 	= "img/icon/iconbar19.png";
    var ICONBAR_DEFAULT_38 	= "img/icon/iconbar38.png";
    var ICONBAR_DISABLED_19 = "img/icon/iconbar-off-19.png";
    var ICONBAR_DISABLED_38 = "img/icon/iconbar-off-38.png";
    var ICONBAR_TRUSTED_19  = "img/icon/iconbar-trusted-19.png";
    var ICONBAR_TRUSTED_38  = "img/icon/iconbar-trusted-38.png";
    var ICONBAR_BLOCKED_19  = "img/icon/iconbar-blocked-19.png";
    var ICONBAR_BLOCKED_38  = "img/icon/iconbar-blocked-38.png";

	var setIconOff = function(tabId) {
        chrome.pageAction.setIcon({
            path: {
                19: ICONBAR_DISABLED_19,
                38: ICONBAR_DISABLED_38
            },
            tabId: tabId
        }, function() {
            chrome.pageAction.show(tabId);
        });
	};

	var setIconTrusted = function(tabId) {
        chrome.pageAction.setIcon({
            path: {
                19: ICONBAR_TRUSTED_19,
                38: ICONBAR_TRUSTED_38
            },
            tabId: tabId
        }, function() {
            chrome.pageAction.show(tabId);
        });
	};

	var setIconBlocked = function(tabId) {
        chrome.pageAction.setIcon({
            path: {
                19: ICONBAR_BLOCKED_19,
                38: ICONBAR_BLOCKED_38
            },
            tabId: tabId
        }, function() {
            chrome.pageAction.show(tabId);
        });
	};

	var setIconDefault = function(tabId) {
        chrome.pageAction.setIcon({
            path: {
                19: ICONBAR_DEFAULT_19,
                38: ICONBAR_DEFAULT_38
            },
            tabId: tabId
        }, function() {
            chrome.pageAction.show(tabId);
        });
	};

	return {
		setIconOff: 	setIconOff,
		setIconTrusted: setIconTrusted,
		setIconBlocked: setIconBlocked,
		setIconDefault: setIconDefault
	};
})();