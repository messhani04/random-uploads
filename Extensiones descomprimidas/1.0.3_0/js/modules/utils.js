var Utils = (function() {
	var isSubDomain = function(url) {
		url = url.replace(new RegExp(/^www\./i), "");
		url = url.replace(new RegExp(/\/(.*)/), "");
		if (url.match(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i))) {
			url = url.replace(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i), "");
		} else if (url.match(new RegExp(/\.[a-z]{2,4}$/i))) {
			url = url.replace(new RegExp(/\.[a-z]{2,4}$/i), "");
		}
		return (url.match(new RegExp(/\./g))) ? true : false;
	};

	var hashCode = function(s) {
  		return s.split("").reduce(function(a, b) {
	    	a = ((a << 5) - a) + b.charCodeAt(0);
	    	return a & a;
  		}, 0);
	};

	var lowerCase_LocaleCompare = function(a, b) {
		return a.toLowerCase().localeCompare(b.toLowerCase());
	};

	var extractDomain = function(url) {
		var domain;
		//find & remove protocol (http, ftp, etc.) and get domain
		if (url.indexOf("://") > -1) {
		    domain = url.split('/')[2];
		} else {
		    domain = url.split('/')[0];
		}
		  //find & remove port number
		domain = domain.split(':')[0];
		var isSubDom = isSubDomain(domain);
		var rootDomain;

		if (!isSubDom) {
		    rootDomain = domain = domain.replace("www.", "");
		} else {
		    rootDomain = domain.substr(domain.indexOf(".") + 1)
		}
		return { 
			domain: domain, 
			isSubDomain: isSubDom, 
			rootDomain: rootDomain, 
			url: url 
		};
	};

	var validateUrl = function(url) {
		var isValidUrl = url && url.indexOf("http") === 0 && url.indexOf("://localhost") === -1;
		var isOptionsUrl = url && chrome.extension.getURL("options.html");

		return isValidUrl || isOptionsUrl;
	};

    var cleanTheUrl = function(url, fromOpt) {
        if (url != undefined && url != "" && url != null) {
            // if it's top url, just return it
            if (url.split("/").length <= 4 && !fromOpt) {
                return url;
            }
            var cleanUrl = url.trim(), urlSimbleIndex = cleanUrl.indexOf("://");
            //remove http:// or https://
            if (urlSimbleIndex === 4 || urlSimbleIndex == 5) {
                cleanUrl = cleanUrl.match(/h.*:\/\/(.*)/)[1];
            }
            //remove last / if exists
            if (cleanUrl.endsWith("/")) {
                cleanUrl = cleanUrl.slice(0, cleanUrl.length - 1);
            }
            //remove the "www."
            if (!Utils.isSubDomain(cleanUrl)) {
                cleanUrl = cleanUrl.replace("www.", "");
            }
            return cleanUrl;
        } else {
            return "";
        }
    };

	return {
		isSubDomain: isSubDomain,
		hashCode: hashCode,
		lowerCase_LocaleCompare: lowerCase_LocaleCompare,
		extractDomain: extractDomain,
		validateUrl: validateUrl,
		cleanTheUrl: cleanTheUrl
	};
})();