var History = (function() {
	var BLOCK_REASONS = {
		URL: 0,
		DOMAIN: 1,
		KEYWORD: 2
	};

	var addItem = function(tabId, url, domain, reason, keyword) {
		if (!Options.GetItem("enableHistory")) { return false; }
		var timeStamp = Date.now();
		if (!isReadyToAddItem(timeStamp, tabId)) { return; }

        var blockHistory = Storage.GetItem("blockHis");
        blockHistory.unshift({
            site: url,
            domain: domain,
            timeStamp: timeStamp,
            reason: reason,
            keyword: keyword || null,
            tabId: tabId
        });

        Storage.SetItem("blockHis", blockHistory);

        return true;
	};

	var getLastItem = function() {
		var blockHistory = Storage.GetItem("blockHis");
		// console.log(blockHistory);
		var index = blockHistory.length-1;
		if (index === -1) { return null; }
		
		return blockHistory[index];
	};

	var getPeviousToLastItem = function() {
		var blockHistory = Storage.GetItem("blockHis");
		var index = blockHistory.length-2;
		if (index === -1 || index === -2) { return null; }
		
		return blockHistory[index];
	};	

	var checkPeviousToLastItem = function(currentTimestamp) {
		var previousToLastItem = getPeviousToLastItem();
		if (!previousToLastItem) { return true; }
		
		return (currentTimestamp - previousToLastItem.timeStamp) > 2000;
	};

	var isReadyToAddItem = function(currentTimestamp, currentTabId) {
		var lastItem = getLastItem();
		if (!lastItem) { return true; }
		if (currentTabId !== lastItem.tabId) { 
			return checkPeviousToLastItem(currentTimestamp); 
		}

		return (currentTimestamp - lastItem.timeStamp) > 2000;
	};

	return {
		addItem: addItem,
		BLOCK_REASONS: BLOCK_REASONS
	};
})();