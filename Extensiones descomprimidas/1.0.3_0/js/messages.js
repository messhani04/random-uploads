chrome.runtime.onConnect.addListener(function(port) {
    if (port.name === "ref") {
	    port.onMessage.addListener(function(msg) {
	        if (msg.message == "bg:get-ref") {
	            // console.log(msg);
	            var url = window.location.href;
	            port.postMessage({ url: url, message: "content:get-ref", referrer: document.referrer });
	        }
	    });
    }
});