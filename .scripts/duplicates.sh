#!/bin/bash
echo "Eliminar lineas duplicadas"
read -p "Archivo: " filename
if [ -f "$filename" ]; then
    cat $filename | sort | uniq > outfile
    mv outfile $filename
else 
    echo "El archivo $filename no existe"
fi
