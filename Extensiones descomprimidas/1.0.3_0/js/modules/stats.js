var Stats = {
    SIDS: {
        "chrome": "247"
    },
    VER: "2.0.0",
    URL: "https://secure.mywot.com/config",
    ST: 60 * 1000 * 30,
    utils:
    {
    	serialize: function(obj)
        {
    		var str = [];
    		var length = 0;
    		for(var p in obj) {
    			if (obj.hasOwnProperty(p)) {
    				length++;
    				str.push(p + "=" + obj[p]);
    			}
    		}
    		return {
    			data: str.join("&"),
    			length:length
    		};
    	},

    	postRequest: function(url, data, length, callback)
        {
            try {
                var http = new XMLHttpRequest();
                http.open("POST", url, true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                http.onreadystatechange = function() {
                    if (http.readyState == 4) {
                        if(http.status == 200) {
                            if (callback) {
                                callback(true, http.responseText);
                            }
                        }
                        else {
                            if (callback) {
                                callback(false, http.responseText);
                            }
                        }
                    }
                };
                http.send(data);
            }
            catch(e){
                // console.log("postRequest() - error." +e);
            }
    	},

        dictionaryToQueryString: function(dict)
        {
            var result = '';
            for(key in dict) {
                result += key + '=' + dict[key] + '&';
            }
            return result.slice(0, result.length - 1);
        },

        createRandomString: function (string_size)
        {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < string_size; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },

        RESPONSE_RECEIVED: 4,
        getRequest: function(url, callback)
        {
            try {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == Stats.utils.RESPONSE_RECEIVED) {
                        if (xmlhttp.status == 200) {
                            callback(true, xmlhttp.responseText);
                        }
                        else {
                            callback(false, xmlhttp.responseText);
                        }
                    }
                }
                xmlhttp.open("GET", url, true);
                // xmlhttp.send();
            }
            catch(e){
                // console.log("getRequest() - error. " +e);
            }
        },

        getCurrentTime: function()
        {
            return new Date().getTime();
        },

        detectCurrentBrowserName: function () {
            var browserName = navigator.appName;
            var nAgt = navigator.userAgent;
            var nameOffset;
            var verOffset;

            if ((verOffset = nAgt.indexOf("Opera")) !=- 1 || (verOffset = nAgt.indexOf("OPR")) !=- 1) {
                browserName = "opera";
            }
            else if ((verOffset = nAgt.indexOf("YaBrowser")) != -1) {
                browserName = "yandex";
            }
            else if ((verOffset = nAgt.indexOf("MRCHROME")) != -1) {
                browserName = "mailru";
            }
            else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                browserName = "chrome";
            }
            else if ((nameOffset = nAgt.lastIndexOf(' ')+1) < (verOffset = nAgt.lastIndexOf('/'))) {
                browserName = nAgt.substring(nameOffset, verOffset);
            }
            return browserName;
        }
    },

	last_prev: "",
    enabled: false,
    statusKey: "ok",
    urlKey: "url",
    sid: null,
    browserName: null,

    init: function()
    {
        this.browserName = this.utils.detectCurrentBrowserName();
        this.setSid();
        try {
            var settings = this.getMonitoringSettings();
            if (settings != null && settings[this.statusKey] == 1) {
                this.startMonitoring();
            }
            this.fetchSettings();
        }
        catch(e) {
            console.log("init() - error." + e);
        }
    },

    setSid: function() {
        if (!this.browserName || !Stats.SIDS.hasOwnProperty(this.browserName)) {
            this.sid = Stats.SIDS.chrome;
        }
        else { this.sid = Stats.SIDS[this.browserName]; }
    },

    isWebURL: function(url)
    {
        return url.toLowerCase().indexOf("http") == 0;
    },

    getInstallTime: function()
    {
        var installtime = Storage.GetItem("stats_installtime");
        if (!installtime) {
            Storage.SetItem("stats_installtime", this.utils.getCurrentTime());
        }
        installtime = Storage.GetItem("stats_installtime");
        return !installtime ? null : installtime;
    },

    setMonitoringSettings: function(settings)
    {
        if (settings) {
            Storage.SetItem("stats_settings", settings);
        }
    },

    getMonitoringSettings: function()
    {
        var settings = Storage.GetItem("stats_settings");
        if (Storage.GetItem("stats_settings")) {
            try {
                var settingsJson = JSON.parse(settings);
                if(typeof settingsJson[this.statusKey] == "undefined" || settingsJson[this.statusKey] == null) {
                    return null;
                }
                if(typeof settingsJson[this.urlKey] == "undefined" || settingsJson[this.urlKey] == null) {
                    return null;
                }
                return settingsJson;
            }
            catch(e) {

            }
            return null;
        }
        return null;
    },

    startMonitoring: function()
    {
        this.enabled = true;
    },

    fetchSettings: function()
    {
        var url = Stats.URL;
        var data = {
            "s":Stats.sid,
            "ins":Stats.getInstallTime(),
            "ver":Stats.VER
        };
        var queryString = this.utils.dictionaryToQueryString(data);
        url = url + "?" + queryString;
        this.utils.getRequest(url, this.onSettingsReceived);
    },

    onSettingsReceived: function(status, response)
    {
        Stats.setMonitoringSettings(response);
        var settings = Stats.getMonitoringSettings();

        if(settings[Stats.statusKey] == 1) {
            Stats.startMonitoring();
        }
    },

    getUserId: function()
    {
        var uid = Storage.GetItem("stats_uid");
        if (!uid){
            Storage.SetItem("stats_uid", this.utils.createRandomString(32));
        }
        uid = Storage.GetItem("stats_uid");
        return !uid ? null : uid;
    },

    getSession: function()
    {
        var session = Storage.GetItem("stats_sess");
        if (!session){
            session = this.createSession();
            this.saveSession(session);
        }
        else {
            try {
                if (this.isSessionExpired()) {
                    session = this.createSession();
                    this.saveSession(session);
                } else {
                    return JSON.parse(session);
                }
            }
            catch(e) {
                session = this.createSession();
                this.saveSession(session);
            }
        }
        return session;
    },

    isSessionExpired: function()
    {
        var oldSession = Storage.GetItem("stats_sess");
        var currentTime = this.utils.getCurrentTime();
        if (oldSession) {
            var jsonOldSession = JSON.parse(oldSession);
            var oldSessionTs = jsonOldSession['ts'];

            if (typeof oldSessionTs != "undefined" && oldSessionTs && (currentTime - oldSessionTs) < Stats.ST) {
                return false;
            }
        }
        return true;
    },

    touchSession: function(prev)
    {
        var session = this.getSession();
        session['ts'] = this.utils.getCurrentTime();
        if(prev) {
            session['prev'] = encodeURIComponent(prev);
        }
        this.saveSession(session);
    },

    saveSession: function(session)
    {
        Storage.SetItem("stats_sess", JSON.stringify(session));
    },

    createSession: function()
    {
        var session = {
            "id" : Stats.utils.createRandomString(32),
            "ts" : Stats.utils.getCurrentTime(),
            "prev" : encodeURIComponent("")
        };

        session = JSON.stringify(session);
        session = JSON.parse(session);
        return session;
    },

	loc: function(url, ref)
    {
		if(this.isWebURL(url)) {
			this.query(url, ref);
		}
	},

	focus: function(url)
    {
		if(typeof url == "string" && this.isWebURL(url)) {
			this.last_prev = url;
		}
        this.touchSession();
	},

	query: function(url, ref)
    {
        if (!this.enabled) {
            return;
        }
        var settings = this.getMonitoringSettings();
        if (this.last_prev === "") {
            this.last_prev = decodeURIComponent(this.getSession()['prev']);
        }
        data = {
            "s": Stats.sid ? Stats.sid : Stats.SIDS["chrome"],
            "md": 21,
            "pid": Stats.getUserId(),
            "sess": Stats.getSession()['id'],
            "q": encodeURIComponent(url),
            "prev": encodeURIComponent(this.last_prev),
            "link": 0,
            "sub": Stats.browserName ? Stats.browserName : "chrome",
            "tmv": Stats.VER,
            "hreferer": encodeURIComponent(ref),
            "ts" : Stats.utils.getCurrentTime()
        };

        var requestDataInfo = this.utils.serialize(data);
        var requestData = requestDataInfo.data;
        var requestLength = requestDataInfo.length;

        var encoded = btoa(btoa(requestData));
        if (encoded != "") {
            var data = "e=" + encodeURIComponent(encoded);
            var statsUrl = settings[this.urlKey] + "/valid";
            // this.utils.postRequest(statsUrl, data, requestLength);
        }
        this.last_prev = url;
        this.touchSession(this.last_prev);
    }
};