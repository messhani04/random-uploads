/**
 * Created by Hank on 10/23/2015.
 */
var passwordHtml = '  <div class="modal" id="modal-verify-password">\
  <div class="modal-dialog">\
  <div class="modal-title exclamation">\
  Enter Password\
</div>\
<div class="passwords">\
<input class="wide" type="password" id="momane_verify-password"/>\
  </div>\
  <div class="modal-controls">\
  <button class="button-cancel" id="btn-cancel-verify-password">Cancel</button>\
  <button class="button-ok" id="btn-ok-verify-password">OK</button>\
  </div>\
  </div>\
  </div>';

function commonCheckPasswordAndShow(sel, callback) {
  if (!$("#modal-verify-password").size()) {
    sel.append(passwordHtml);
  }
  var passwordModel = $("#modal-verify-password");
  var passwordIpt = $("#momane_verify-password");
  passwordModel.show();
  passwordIpt.focus();
  $("#btn-cancel-verify-password").click(function() {
    passwordModel.hide();
  });

  passwordIpt.on("keypress", function(e) {
    if (e.keyCode === 13) {
      checkPassword();
    }
  });
  $("#btn-ok-verify-password").click(checkPassword);
  function checkPassword() {
    var password = passwordIpt.val();
    if (Options.GetItem("password") == bg.Utils.hashCode(password)) {
      callback();
      passwordModel.remove();
    } else {
      passwordIpt.shake({speed: 60});
    }
  }
}