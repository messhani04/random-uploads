var bg = chrome.extension.getBackgroundPage();
var Options = bg.Options;
var Storage = bg.Storage;
var Password = bg.Password;

var buttonNames = {
  exticon: {
    show: "Show Address Bar Icon",
    hide: "Hide Address Bar Icon"
  },
  bypassBlock: {
    show: "Show 'Allow Once' and 'Trust' Buttons",
    hide: "Hide 'Allow Once' and 'Trust' Buttons"
  },
  historyBlock: {
    show: ""
  },
  blockReason: {
    show: "Show Block Reason",
    hide: "Hide Block Reason"
  },
  toggleFilter: {
    enable: "Enable Filter",
    disable: "Disable Filter"
  }
};


var passwordHtml = '  <div class="modal" id="modal-verify-password">\
  <div class="modal-dialog">\
  <div class="modal-title exclamation">\
  Enter Password\
</div>\
<div class="passwords">\
  <label for="password">Enter Password</label>\
<input class="wide" type="password" id="momane_verify-password"/>\
  </div>\
  <div class="modal-controls">\
  <button class="button-cancel" id="btn-cancel-verify-password">Cancel</button>\
  <button class="button-ok" id="btn-ok-verify-password">OK</button>\
  </div>\
  </div>\
  </div>';

function showMessage() {
  $('.message').fadeIn(500, function() {
    setTimeout(function() {
      $('.message').fadeOut(500);
    }, 2000);
  });
}

function renderHistoryTable() {
  var blockHis = Storage.GetItem("blockHis");
  if (blockHis && blockHis.length > 0) {
    var str = "";
    $.each(blockHis, function(i, his) {
      var reasonTitle = '', reasonName = "";
      switch (his.reason) {
        case 0:
          reasonTitle = "\'" + his.url + "\' is a Blocked URL";
          reasonName = "\'<div class='reasonName'>" + his.url + "</div>\' is a blocked URL";
          break;
        case 1:
          reasonName = "\'<div class='reasonName'>" + his.domain + "'" + "</div>\ is a blocked site";
          reasonTitle = "\'" + his.domain + "'" + "\ is a blocked site";
          break;
        case 2:
          reasonTitle = "\'<div class='reasonName'>" + his.keyword + "</div>\' is a blocked keyword"
          reasonName = "\'<div class='reasonName'>" + his.keyword + "'" + "</div>\ is a blocked keyword";
          break;
      }
      str += '<tr rel="' + i + '"><td title="' + his.site + '" class="truncate">' + his.site + '</td>' +
        '<td>' + moment(his.timeStamp).format('MMM Do, YYYY h:mm a') + '</td>' +
        '<td title="' + reasonTitle + '" class="truncate-reason">' + reasonName + '</td>' +
        '<td rel="' + i + '" class="removeHis" title="Remove this record"><div></div></td></tr>';
    });
    $("#historyRecord").show().find("tbody").html(str);
  } else {
    $("#historyRecord").hide();
  }
}

function isListEmpty(list_name) {
  var items = Storage.GetItem(list_name);
  return items.length === 0;
}

function addListsEvents() {
  window.addEventListener("listItemAdded", function(event) {
    var listName = event.detail.listName;
    var items = Storage.GetItem(listName);
    if (items.length === 1) {
      var plusListName = getPlusListByStorageList(listName);
      setListPlusOff(plusListName);
      // console.log("first item added " + listName);
    }
  }, false);

  window.addEventListener("listEmptied", function(event) { 
    var listName = event.detail.listName;
    var plusListName = getPlusListByStorageList(listName);
    setListPlusOn(plusListName);
    // console.log(event.detail);
  }, false);

  $(".add-to-list-input").on("focus", function(event) {
    var id = $(event.currentTarget).attr("id");
    if (id === "add-trusted-site" && isListEmpty("trusted_sites")) {
      setListPlusActive("trusted-sites-list");
    }
    if (id === "add-blocked-site" && isListEmpty("blocked_sites")) {
      setListPlusActive("blocked-sites-list");
    }
    if (id === "add-blocked-keyword" && isListEmpty("blocked_keywords")) {
      setListPlusActive("keywords-list");
    }        
  });

  $(".add-to-list-input").on("focusout", function(event) {
    var id = $(event.currentTarget).attr("id");
    if (id === "add-trusted-site" && isListEmpty("trusted_sites")) {
      setListPlusOn("trusted-sites-list");
    }
    if (id === "add-blocked-site" && isListEmpty("blocked_sites")) {
      setListPlusOn("blocked-sites-list");
    }
    if (id === "add-blocked-keyword" && isListEmpty("blocked_keywords")) {
      setListPlusOn("keywords-list");
    }
  });

  $(".list_plus_image").on("click", function(event) {
    var parentId = $(event.currentTarget).parent().attr("id");
    if (parentId === "trusted-sites-list") { $("#add-trusted-site").focus(); }
    if (parentId === "blocked-sites-list") { $("#add-blocked-site").focus(); }
    if (parentId === "keywords-list") { $("#add-blocked-keyword").focus(); }
  });
}

function initListPluses() {
  var trusted_sites = Storage.GetItem("trusted_sites");
  var blocked_sites = Storage.GetItem("blocked_sites");
  var blocked_keywords = Storage.GetItem("blocked_keywords");
  if (trusted_sites.length === 0) { setListPlusOn("trusted-sites-list"); }
  else { setListPlusOff("trusted-sites-list"); }
  if (blocked_sites.length === 0) { setListPlusOn("blocked-sites-list"); }
  else { setListPlusOff("blocked-sites-list"); }
  if (blocked_keywords.length === 0) { setListPlusOn("keywords-list"); }
  else { setListPlusOff("keywords-list"); }
}

function getPlusListByStorageList(storageListName) {
  if (storageListName === "trusted_sites") { return "trusted-sites-list"; }
  if (storageListName === "blocked_sites") { return "blocked-sites-list"; }
  if (storageListName === "blocked_keywords") { return "keywords-list"; }

  return null;
}

function setListPlusOn(listName) {
  $("#" + listName + " .list_plus_image").removeClass("off");
  $("#" + listName + " .list_plus_image").removeClass("active");
  $("#" + listName + " .list_plus_image").addClass("on");
}

function setListPlusOff(listName) {
  $("#" + listName + " .list_plus_image").removeClass("on");
  $("#" + listName + " .list_plus_image").removeClass("active");
  $("#" + listName + " .list_plus_image").addClass("off");
}

function setListPlusActive(listName) {
  $("#" + listName + " .list_plus_image").removeClass("off");
  $("#" + listName + " .list_plus_image").removeClass("on");
  $("#" + listName + " .list_plus_image").addClass("active");
}

function init() {
  addListsEvents();
  initListPluses();
  if (!Options.GetItem("active")) {
    $(".filterOff").show();
  }

  if (Storage.GetItem("blockHis")) {
    renderHistoryTable();
  }
  $("#btn-on-off").text(Options.GetItem("active") ? 'Disable Filter' : 'Enable Filter');
  setList('#list-trusted-sites', 'trusted_sites');
  setList('#list-blocked-sites', 'blocked_sites');
  setList('#list-blocked-keywords', 'blocked_keywords');
  var historySettings = $('.history-settings');
  if (Options.GetItem("password_active")) {
    setPasswordSettingsUI(true);
    $('#password-settings-cover').css('display', 'none');
    $('.bypass-block').show();
  } else {
    setPasswordSettingsUI(false);
    $('#password-settings-cover').css('display', 'block');
  }
  if (Options.GetItem("enableHistory")) {
    $('.history-settings').removeClass('off');
    $('#history-settings-cover').css('display', 'none');
    historySettings.find('.controls-block').show();
    historySettings.find('.info').hide();
    $("#hisSlider").prop("checked", true);
    if (Storage.GetItem("blockHis")) {
      var blockHis = Storage.GetItem("blockHis");
      if (blockHis && blockHis.length > 0) {
        historySettings.find('#historyRecord').show();
      }
    }

  } else {
    historySettings.find(".status-block").css("padding-left", "45%");
    historySettings.addClass('off');
    historySettings.find('#historyRecord').hide();
  }
  $('#slider').prop('checked', Options.GetItem("password_active"));
  if (Options.GetItem("enable_bypass")) {
    $("#btn-set-bypass").text(buttonNames.bypassBlock.hide);
  } else {
    $("#btn-set-bypass").text(buttonNames.bypassBlock.show);
  }
  if (!Options.GetItem("show_block_reason")) {
    $("#btn-set-blockreason").text(buttonNames.blockReason.show);
  }
  if (!Options.GetItem("showIcon")) {
    $("#btn-hide").text(buttonNames.exticon.show);
  }
}

function setList(selector, list_name) {
  // var items = bg.config(list_name, []);
  var items = Storage.GetItem(list_name);
  var ul = $(selector);
  ul.empty();
  for (var i = 0; i < items.length; i++) {
    if (items[i] !== "") {
      var li = $('<li>');
      var div = $('<div>', {
        class: "item",
        title: items[i]
      }).text(items[i]).appendTo(li);
      var button = $('<button>', {
        class: "remove",
        i: i
      }).click(function() {
        bg.removeFromListByIndex(list_name, $(this).attr('i'), function() {
          if (isListEmpty(list_name)) {
            var event = new CustomEvent('listEmptied', { 'detail': { listName: list_name } });
            window.dispatchEvent(event);
          }
          setList(selector, list_name);
          showMessage();
        });
      });
      li.append(div);
      li.append(button);
      ul.append(li);
    }
  }
}

function addTrustedSite() {
  var val = $("#add-trusted-site").val();
  val = val.trim();
  if (val) {
    bg.addToList('trusted_sites', val.toLowerCase());
    setList('#list-trusted-sites', 'trusted_sites');
    showMessage();
    $("#add-trusted-site").val('');
    $("#add-trusted-site").focus();
  }
}

function addBlockedSite() {
  var val = $("#add-blocked-site").val();
  val = val.trim();
  if (val) {
    bg.addToList('blocked_sites', val.toLowerCase());
    setList('#list-blocked-sites', 'blocked_sites');
    showMessage();
    $("#add-blocked-site").val('');
    $("#add-blocked-site").focus();
  }
}

function addBlockedKeyword() {
  var val = $("#add-blocked-keyword").val();
  val = val.trim();
  if (val) {
    bg.addToList('blocked_keywords', val.toLowerCase());
    setList('#list-blocked-keywords', 'blocked_keywords');
    showMessage();
    $("#add-blocked-keyword").val('');
    $("#add-blocked-keyword").focus();
  }
}

function setPasswordSettingsUI(value) {
  var passwordSettings = $('.password-settings');
  var passSetRightBlock = $('.right-block');
  var passSetLeftBlock = $(".left-block");
  var btnSetPass = $("#btn-set-password");
  var statusBlock = $(".status-block");
  if (value === true) {
    passwordSettings.removeClass('off');
    btnSetPass.removeClass('off');
    passSetRightBlock.removeClass('off');
    passSetLeftBlock.removeClass('off');
    statusBlock.removeClass('off');
  } else if (value === false) {
    passwordSettings.addClass('off');
    btnSetPass.addClass('off');
    passSetRightBlock.addClass('off');
    passSetLeftBlock.addClass('off');
    statusBlock.addClass('off');
  }
}


function checkPasswordAndShow(passCB, cancelCB, hideCancel) {
  if (!$("#modal-verify-password").size()) {
    $(".wot-options-content").append(passwordHtml);
  }
  var passwordModel = $("#modal-verify-password");
  var passwordIpt = $("#momane_verify-password");
  if (hideCancel) {
    $("#btn-cancel-verify-password").hide();
  }
  passwordModel.show();
  passwordIpt.focus();
  $("#btn-cancel-verify-password").click(function() {
    passwordModel.remove();
    if (cancelCB) {
      cancelCB();
    } else {
      if (Options.GetItem("password_active")) {
        $("#slider").prop("checked", true);
      } else {
        $("#slider").prop("checked", false);
      }
    }
  });
  passwordIpt.on("keypress", function(e) {
    if (e.keyCode === 13) {
      checkPassword();
    }
  });
  $("#btn-ok-verify-password").click(checkPassword);
  function checkPassword() {
    var password = passwordIpt.val();
    if (bg.Password.check(password)) {
      passCB();
      passwordModel.remove();
    } else {
      passwordIpt.shake({speed: 60});
    }
  }
}
/* events */

$("#btn-on-off").on("click", function(e) {
  if (Options.GetItem("password_active")) {
    checkPasswordAndShow(function() {
      bg.switchOnOff(function() {
        //filterStatus();
      });
    });
  } else {
    bg.switchOnOff(function() {
      //filterStatus();
    });
  }
});

chrome.runtime.onMessage.addListener(function(req, sender, sr) {
  if (req.cmd == "onOff") {
    if (Options.GetItem("active")) {
      $(".filterOff").fadeOut(700);
    } else {
      $(".filterOff").fadeIn(700);
    }
    $("#btn-on-off").text(Options.GetItem("active") ? buttonNames.toggleFilter.disable : buttonNames.toggleFilter.enable);
    showMessage();
  }
});

$("#btn-add-trusted-site").click(function() {
  addTrustedSite();
  var list_name = "trusted_sites";
  if (!isListEmpty(list_name)) {
    var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
    window.dispatchEvent(event);
  }
});

$("#btn-add-blocked-site").click(function() {
  addBlockedSite();
  var list_name = "blocked_sites";
  if (!isListEmpty(list_name)) {
    var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
    window.dispatchEvent(event);
  }  
});

$("#btn-add-blocked-keyword").click(function() {
  addBlockedKeyword();
  var list_name = "blocked_keywords";
  if (!isListEmpty(list_name)) {
    var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
    window.dispatchEvent(event);
  }  
});

$("#add-trusted-site").keypress(function(e) {
  if (e.which == 13) {
    addTrustedSite();
    var list_name = "trusted_sites";
    if (!isListEmpty(list_name)) {
      var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
      window.dispatchEvent(event);
    }    
  }
});

$("#add-blocked-site").keypress(function(e) {
  if (e.which == 13) {
    addBlockedSite();
    var list_name = "blocked_sites";
    if (!isListEmpty(list_name)) {
      var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
      window.dispatchEvent(event);
    }      
  }
});

$("#add-blocked-keyword").keypress(function(e) {
  if (e.which == 13) {
    addBlockedKeyword();
    var list_name = "blocked_keywords";
    if (!isListEmpty(list_name)) {
      var event = new CustomEvent('listItemAdded', { 'detail': { listName: list_name } });
      window.dispatchEvent(event);
    }    
  }
});


$(".password-settings .slider label").click(function() {
  if (Options.GetItem("password_active")) {
    checkPasswordAndShow(function() {
      setPasswordSettingsUI(false);
      Options.SetItem("password_active", false);
      Options.SetItem("password", null);
      Options.SetItem("enable_bypass", true);
      Options.SetItem("show_block_reason", true);
      showMessage();
    }, null, false);
  } else {
    $('#modal-password').show();
    $("#password").val("");
    $("#password2").val("");
    $("#password").focus();
    $(".oldPassword").hide();
  }
});

//event fro enable/disable history
$(".history-settings .slider label").click(function() {
  var historySettins = $('.history-settings');
  if (Options.GetItem("enableHistory")) {
    historySettins.addClass('off');
    historySettins.find(".status-block").css("padding-left", "45%");
    historySettins.find('.controls-block').hide();
    $("#historyRecord").hide();
    historySettins.find(".info").show();
    Options.SetItem("enableHistory", false);
    showMessage();
  } else {
    historySettins.removeClass('off');
    historySettins.find(".status-block").css("padding-left", "");
    historySettins.find('.controls-block').show();
    renderHistoryTable();
    historySettins.find(".info").hide();
    Options.SetItem("enableHistory", true);
  }
});

//event for remove history icon clicking
$("body").on("click", ".removeHis", function() {
  var id = parseInt($(this).attr("rel"));
  var blockHis = Storage.GetItem("blockHis");
  if (blockHis.length >= id) {
    blockHis.splice(id, 1);
    if (blockHis.length >= 1) {
      Storage.SetItem("blockHis", blockHis);
      renderHistoryTable();
    } else {
      $("#historyRecord").find("table").fadeIn().html("");
      Storage.SetItem("blockHis", []);
    }
  }
});

//position of the history hint
$(".hisinfo").hover(function() {
  var left = 380;
  if ($(".history-settings.off").length === 0) {  // block history is on
    left = 86;
  }
  var that = $(this), pos = that.offset();
  var hint = $(".hisHint");
  hint.css({
    top: pos.top - 148,
    left: left
  }).show();
}, function(){
  $(".hisHint").hide();
});

// clear history
$("#btn-clear-history").click(function() {
  var isClear = confirm("Are you sure you want to permanently clear the block history?");
  if (isClear) {
    $("#historyRecord").find("table").fadeIn().html("");
    Storage.SetItem("blockHis", [])
  }
});

//open backup
$("#btn-backup").click(function() {
  var bo = $(".backupOptions");
  if (bo.is(":visible")) {
    bo.hide();
  } else {
    bo.css("display", "inline-block");
  }
});

$("#btn-export").click(function() {
  var fileName = "Filter by WOT Backup";
  var finalStr = "";
  finalStr += "Trusted Site," + Storage.GetItem("trusted_sites").join(",") + "\n";
  finalStr += "Blocked Site," + Storage.GetItem("blocked_sites").join(",") + "\n";
  finalStr += "Blocked Keyword," + Storage.GetItem("blocked_keywords").join(",") + "\n";

  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/csv;charset=latin-1,' + encodeURIComponent(finalStr));
  pom.setAttribute('download', fileName + ' - ' + moment(Date.now()).format('MMM Do, YYYY') + '.csv');
  pom.click();
  $(pom).remove();
});


$("#btn-import").click(function() {
  $("#txtFileUpload").click();
});

$("#txtFileUpload").change(function(e) {
  var data = null;
  var file = e.target.files[0];
  try {
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function(event) {
      var csvData = event.target.result;
      if (csvData && csvData.length > 0) {
        var datas = csvData.split("\n");
        if (datas.length === 4) {
          datas.splice(3,1);
        }
        if (datas.length === 3) {
          var ts = datas[0].split(","),
            bs = datas[1].split(","),
            bk = datas[2].split(",");
          if (ts[0] == "Trusted Site" && bs[0] == "Blocked Site" && bk[0] == "Blocked Keyword") {
            ts.shift();
            bs.shift();
            bk.shift();

            Storage.SetItem("trusted_sites", cleanArr(ts));
            Storage.SetItem("blocked_sites", cleanArr(bs));
            Storage.SetItem("blocked_keywords", cleanArr(bk));
            window.location.reload();
          } else {
            alert("Data is malformed 1.");
          }
        } else {
          alert("Data is malformed 2.");
        }
      } else {
        alert("Data is malformed 3.");
      }
    };
    reader.onerror = function() {
      alert('Unable to read ' + file.fileName);
    };
  } catch (e) {
    alert("Data is malformed.");
  }
});
function cleanArr(arr) {
  if (arr && arr.length > 0) {
    var cleanArr = [];
    $.each(arr, function(i, a) {
      if (a != "") {
        cleanArr.push(a);
      }
    });
    return cleanArr;
  } else {
    return [];
  }
}


function showPasswordAction() {
  setPasswordSettingsUI(true);
  $("#slider").prop("checked", true);
}

$('#btn-set-bypass').click(function() {
  var that = $(this), enable = that.text().indexOf("S") === 0;
  Options.SetItem("enable_bypass", enable);
  showMessage();
  if (enable) {
    that.text(buttonNames.bypassBlock.hide);
  } else {
    that.text(buttonNames.bypassBlock.show).css("color", "#afafaf");
  }
});

$('#btn-set-password').click(function() {
  if (Options.GetItem("password_active")) {
    $(".oldPassword").show();
  }
  $('#modal-password').css('display', 'block');
  $('#password').val('');
  $('#password2').val('');
  $('#oldPassword').val('');
});

$('#btn-reset').click(function() {
  if (Options.GetItem("password_active")) {
    checkPasswordAndShow(function() {
      $('#modal-reset').css('display', 'block');
    }, null, false);
  } else {
    $('#modal-reset').css('display', 'block');
  }
});

$('.modal-close, .button-cancel').click(function() {
  $(this).closest('.modal').css('display', 'none');
});

$('#btn-ok-modal-reset').click(function() {
  Options.SetToDefault();
  Storage.SetItem("trusted_sites", []);
  Storage.SetItem("blocked_sites", []);
  Storage.SetItem("blocked_keywords", []);
  Storage.SetItem("blockHis", []);
  window.location.reload();
});

$('#btn-cancel-modal-password, #btn-close-modal-password').click(function() {
  $('#password').val('');
  $('#password2').val('');
  if (Options.GetItem("password_active")) {
    $("#slider").prop("checked", true);
  } else {
    $("#slider").prop("checked", false);
  }
});

$('#btn-ok-modal-password').click(setPassWord);
$('#password2').on("keypress", function(e) {
  if (e.keyCode === 13) {
    setPassWord();
  }
});
$('#btn-set-blockreason').click(function(e) {
  var that = $(this), enable = that.text().indexOf("S") === 0;
  Options.SetItem("show_block_reason", enable);
  showMessage();
  if (enable) {
    that.text(buttonNames.blockReason.hide);
  } else {
    that.text(buttonNames.blockReason.show).css("color", "#afafaf");
  }
});
$('#btn-hide').click(function(e) {
  var that = $(this), enable = that.text().indexOf("S") === 0;
  Options.SetItem("showIcon", enable);
  chrome.runtime.sendMessage({message: "toggleIcon", showIcon: enable});
  showMessage();
  if (enable) {
    that.text(buttonNames.exticon.hide);
  } else {
    that.text(buttonNames.exticon.show);
  }
});

function setPassWord() {
  var password1 = $('#password').val();
  var password2 = $('#password2').val();
  var oldPasswordIpt = $("#oldPassword");
  if (oldPasswordIpt.is(":visible")) {
    var oldPassword = oldPasswordIpt.val();
    if (bg.Utils.hashCode(oldPassword) != Options.GetItem("password")) {
      $("#oldPassword").shake({speed: 60});
      return;
    }
  }
  if (password1!== "" && password1 == password2) {
    Options.SetItem("password", password1 ? bg.Utils.hashCode(password1) : '');
    Options.SetItem("password_active", true);
    showPasswordAction();
    showMessage();
    $("#modal-password").hide();
  } else {
    $('#password').shake({speed: 60});
    $('#password2').shake({speed: 60});
  }
}

document.getElementById("btn-facebook").addEventListener("click", function(e) {
  chrome.tabs.create({url: "https://www.facebook.com/weboftrust"});
});
document.getElementById("btn-twitter").addEventListener("click", function(e) {
  chrome.tabs.create({url: "https://twitter.com/web_of_trust"});
});

// password protection
if (Options.GetItem("password_active") && Options.GetItem("password")) {
  checkPasswordAndShow(function() {
    init();
  }, null, true);
} else {
  init();
}

if (!(Storage.GetItem("guideShow"))) {
  Storage.SetItem("guideShow", true);
  $("#modal-guide").show();
}

$("#btn-start-guide").click(function() {
  var that = $(this), step = that.attr("rel"),
    modelContainer = $("#modal-guide"),
    modal = modelContainer.find(".modal-dialog"),
    title = modal.find(".modal-title"),
    content = $(".guideContent"),
    passwdIpt1 = $("#guidePass1"),
    passwdIpt2 = $("#guidePass2"),
    cancelBtn = $("#btn-guide-cancel"),
    pageDiv = $(".guide-page");

  switch (step) {
    case "1":
      modal.fadeOut(function() {
        title.text("Load Adult Content Filter?");
        content.html("If you prefer, we can automatically enable adult<br/> content blocking in your Filter.<br/><br/>Also, consider enabling Filter in Incognito<br/>mode from Chrome's extension settings.");
        that.text("Load adult content filter").attr("rel", "2");
        cancelBtn.show();
        pageDiv.text("2 of 3");
      }).fadeIn();
      break;
    case "2":
      locadDefaultFilter();
      gotoStep3();
      break;
    case "3":
      var password1 = passwdIpt1.val(),
        password2 = passwdIpt2.val();
      if (password1 !== "" && password1 === password2) {
        $(".passwordStatus").text(" ");
        Options.SetItem("password", password1 ? bg.Utils.hashCode(password1) : '');
        Options.SetItem("password_active", true);
        showPasswordAction();
        modelContainer.fadeOut();
        init();
      } else {
        $(".passwordStatus").text("Oops! Your passwords don't match.");
      }
      break;
  }
  cancelBtn.click(function() {
    var cthat = $(this), step = cthat.attr("rel");
    switch (step) {
      case "2":
        gotoStep3();
        break;
      case "3":
        modelContainer.fadeOut();
        initListPluses();
        Options.SetItem("active", true);
        init();
        break;
    }
  });

  function gotoStep3() {
    modal.fadeOut(function() {
      that.text("Set Password").attr("rel", "3").css("background-color", "#7ec437");
      title.text("Lock it Down!");
      content.html("Set a password to avoid unwanted changes <br/> and bypasses.");
      $(".guideInlinePassword").show();
      cancelBtn.text("No Password for Me").attr("rel", "3");
      pageDiv.text("3 of 3");
    }).fadeIn();
  }

});

function locadDefaultFilter() {
  var _default_trusted_sites = ['mywot.com', 'facebook.com', 'twitter.com', 'google.com'];
  var _default_blocked_sites = ['pornhub.com', 'redtube.com', 'keezmovies.com', 'extremetube.com', 'adultfriender.com', 'youporn.com', 'porn.com', 'tub8.com', 'thumbzilla.com', 'spankwire.com', 'xtube.com', 'pornmd.com'];
  var _default_blocked_keywords = ['porn', 'pornography', 'nude', 'blowjob', 'fetish', 'milf', 'sex', 'gone wild', 'gore'];
  Options.SetToDefault();
  Storage.SetItem("trusted_sites", _default_trusted_sites);
  Storage.SetItem("blocked_sites", _default_blocked_sites);
  Storage.SetItem("blocked_keywords", _default_blocked_keywords);
}

chrome.runtime.onMessage.addListener(function(req, sender, sr) {
  if (req.message == "toggleFromPopupIcon") {
    $("#btn-hide").text(req.enable ? buttonNames.exticon.hide : buttonNames.exticon.show);
  }
});