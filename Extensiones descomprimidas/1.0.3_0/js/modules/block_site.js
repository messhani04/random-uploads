var BlockSite = (function() {
	var init = function() {
		addWebRequestListener();
		setTabsOnUpdatedListener();
	};

	var addWebRequestListener = function() {
		var filter = {
			types: ["main_frame"],
			urls: []
		};
		chrome.webRequest.onHeadersReceived.addListener(function(details) {
			// console.log("onHeadersReceived");
			if (details.frameId === 0 && details.url && Options.GetItem("active")) {
				// console.log("onHeadersReceived frame 0");
				// console.log(details);
				var url = details.url;
				var domain = Utils.extractDomain(url).domain;
				var tabId = details.tabId;

				if (SiteVerifier.isTrustedSite(url)) { return; }

				var blockedDetails = SiteVerifier.isBlockedSite(url, true);
				// console.log(blockedDetails);
				if (!blockedDetails || !blockedDetails.isBlocked) { return; }
				// console.log(Utils.extractDomain(url));
				pendingBlocks[tabId] = {
					tabId: tabId,
					populateBlockPopup: true, 
					url: url,
					domain: domain,
					blockReason: blockedDetails.reason
				};
				return { redirectUrl: chrome.extension.getURL("res/blocked_options.html") };
			}
		}, filter, ["blocking"]);
	};

	var setTabsOnUpdatedListener = function() {
		chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
			if (changeInfo.status !== "complete") { return; }
			if (pendingBlocks.hasOwnProperty(tabId)) {
				var blockInfo = pendingBlocks[tabId];
				delete pendingBlocks[tabId];
				History.addItem(tabId, 
					blockInfo.url, 
					blockInfo.domain, 
					blockInfo.blockReason
				);
				Icons.setIconBlocked(tabId);
				// console.log("onUpdated");
				// console.log(changeInfo);
				// console.log("sending message");
				chrome.tabs.sendMessage(tabId, {
					populateBlockPopup: blockInfo.populateBlockPopup, 
					url: blockInfo.url,
					domain: blockInfo.domain,
					blockReason: blockInfo.blockReason
				});
			}
		});
	};

	init();
})();