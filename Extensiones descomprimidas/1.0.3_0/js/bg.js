// var Filter = new function() {

    // consts
    var MESSAGES = {
        GET_OPTIONS             : "options",
        GET_BLOCKED_KEYWORDS    : "getBlockedKeywords",
        IS_TRUSTED_SITE         : "isTrustedSite",
        IS_BLOCKED_SITE         : "isBlockedSite",
        REPORT_KEYWORD_BLOCKING : "reportKeywordBlocking",
        CHECK_PASSWORD          : "checkPassword",
        ADD_TRUST               : "addTrust",
        BYPASS                  : "bypass",
        SET_ICON                : "setIcon",
        TOGGLE_FROM_POPUP_ICON  : "toggleFromPopupIcon",
        TOGGLE_ICON             : "toggleIcon"
    };

    var Options = new LocalStorageAdvanced(
        "user_options",
        {
            active: true,
            password_active: false,
            password: '',
            enable_bypass: true,
            show_block_reason: true,
            showIcon: true,
            enableHistory: false            
        }
    ); // defined as a module

    var Storage = new LocalStorageAdvanced(
        "addon_storage", 
        {
            trusted_sites: [],
            blocked_sites: [],
            blocked_keywords: [],
            blockHis: [],
            guideShow: false,
            stats_installtime: null,
            stats_settings: null,
            stats_uid: null,
            stats_sess: null
        }
    );

    var bypassedDomain = [];
    var pendingBlocks = {};
    var loc_map = {};
    var fetching = false;

    function init() {
        setChromeEventListeners();
        Stats.init();
    }

    function addBypassedDomain(domain) {
        if (!domain) { return; }
        bypassedDomain.push(Utils.extractDomain(domain).domain);
    }
    
    function addToList(list_name, strList) {
        var newList = strList.split(",");
        var list = Storage.GetItem(list_name);
        var listLen = newList.length;
        for (var i = listLen - 1; i >= 0; i--) {
            var str = Utils.cleanTheUrl(newList[i], true);
            if (list.indexOf(str) === -1) {
                list.unshift(str);
            }
        }
        Storage.SetItem(list_name, list);
    }

    function removeFromListByIndex(list_name, i, callback) {
        var list = Storage.GetItem(list_name);
        if (i >= 0) {
            list.splice(i, 1);
            Storage.SetItem(list_name, list);
            callback();
        }
    }

    function removeFromList(list_name, str) {
        var list = Storage.GetItem(list_name);
        var i = list.indexOf(str);
        if (i >= 0) {
            list.splice(i, 1);
            Storage.SetItem(list_name, list);
        }
    }    

    function reloadTab(tab, url) {
        chrome.tabs.update(tab.id, {
            url: url || tab.url
        });
    }

    function switchOnOff() {
        var active = Options.GetItem("active");
        if (active !== null) { Options.SetItem("active", !active); }
        chrome.runtime.sendMessage({cmd: "onOff"});
        chrome.tabs.query({}, function(tabs) {
            var i;
            for (i = 0; i < tabs.length; i++) {
                setBrowserActionIcon(tabs[i], false);
            }
        });
    }

    function trustSite(tab) {
        var domain = Utils.extractDomain(tab.url).domain;
        var trusted_sites = Storage.GetItem("trusted_sites");
        var blocked_sites = Storage.GetItem("blocked_sites");

        if (trusted_sites.indexOf(domain) == -1) {
            trusted_sites.unshift(domain);
        }

        var i = blocked_sites.indexOf(domain);
        if (i >= 0) {
            blocked_sites.splice(i, 1);
        }

        Storage.SetItem("trusted_sites", trusted_sites);
        Storage.SetItem("blocked_sites", blocked_sites);

        //setBrowserActionIcon(tab);
        chrome.tabs.reload(tab.id);
    }

    function blockSite(tab) {
        var domain = Utils.extractDomain(tab.url).domain;
        var trusted_sites = Storage.GetItem("trusted_sites");
        var blocked_sites = Storage.GetItem("blocked_sites");

        if (blocked_sites.indexOf(domain) === -1) {
            blocked_sites.unshift(domain);
        }

        var i = trusted_sites.indexOf(domain);
        if (i >= 0) {
            trusted_sites.splice(i, 1);
        }

        Storage.SetItem("trusted_sites", trusted_sites);
        Storage.SetItem("blocked_sites", blocked_sites);

        //setBrowserActionIcon(tab);
        chrome.tabs.reload(tab.id);
    }

    function setBrowserActionIcon(tab) {
        // console.log("setBrowserActionIcon");
        if (!Options.GetItem("active")) {
            // console.log("setting icon off");
            // console.log(tab);
            Icons.setIconOff(tab.id);
            return;
        }
        // console.log(tab.url);
        if (tab.url === chrome.extension.getURL("res/blocked_options.html")) {
            Icons.setIconBlocked(tab.id);
        } else if (SiteVerifier.isTrustedSite(tab.url)) {
            Icons.setIconTrusted(tab.id);
        } else if (SiteVerifier.isBlockedSite(tab.url).isBlocked) {
            // console.log("setting blocked");
            Icons.setIconBlocked(tab.id);
        } else {
            // console.log("default");
            Icons.setIconDefault(tab.id);
        }
    }

    function setChromeEventListeners() {
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                var tab = sender.tab;
                var tabId = sender.tab.id;
                var url = request.url || sender.tab.url;
                var domain = request.domain || Utils.extractDomain(sender.tab.url).domain;
                var keyword = request.keyword || null;

                switch (request.message) {
                    case MESSAGES.GET_OPTIONS:
                        sendResponse(Options.Get());
                    break;

                    case MESSAGES.GET_BLOCKED_KEYWORDS:
                        sendResponse(Storage.GetItem("blocked_keywords"));
                    break;

                    case MESSAGES.IS_TRUSTED_SITE:
                        sendResponse({result: SiteVerifier.isTrustedSite(url, false)});
                    break;

                    case MESSAGES.IS_BLOCKED_SITE:
                        var blockedDetails = SiteVerifier.isBlockedSite(url, true);
                        if (!blockedDetails || !blockedDetails.isBlocked) { 
                            sendResponse({ isBlockedSite: false });
                            return;
                        }
                        History.addItem(tabId, url, domain, blockedDetails.reason);
                        Icons.setIconBlocked(tabId);
                        sendResponse({ isBlockedSite: true, domain: domain, url: url, blockReason: blockedDetails.reason});
                    break;

                    case MESSAGES.REPORT_KEYWORD_BLOCKING:
                        // console.log("MESSAGES.REPORT_BLOCKING");
                        Icons.setIconBlocked(tabId);
                        History.addItem(tabId, url, domain, History.BLOCK_REASONS.KEYWORD, keyword);
                    break;

                    case MESSAGES.CHECK_PASSWORD:
                        var passed = Password.check(request.password);
                        // if (passed) { 
                            // addBypassedDomain(domain); 
                            // reloadTab(tab);
                        // }
                        // if (request.tempPass) { addBypassedDomain(domain); }
                        // if (passed) { reloadTab(tab); }
                        sendResponse({passed: passed});
                    break;
                    case MESSAGES.ADD_TRUST:
                        // console.log(sender);
                        // console.log(domain);
                        addToList('trusted_sites', domain);
                        removeFromList("blocked_sites", domain);
                        reloadTab(tab, url);
                    break;
                    case MESSAGES.BYPASS:
                        addBypassedDomain(domain);
                        // console.log(sender);
                        reloadTab(tab, url);
                        // sendResponse({bypassed: true});
                    break;
                    case MESSAGES.TOGGLE_FROM_POPUP_ICON: //same action with toggleIcon
                    case MESSAGES.TOGGLE_ICON:
                        Options.SetItem("showIcon", request.showIcon);
                        if (!request.showIcon) {
                            chrome.tabs.query({}, function(tabs) {
                                var i = 0, tabsLen = tabs.length;
                                for (i; i < tabsLen; i++) {
                                    chrome.pageAction.hide(tabs[i].id);
                                }
                            });
                        } else {
                            setBrowserActionIcon(tab);
                        }
                    break;
                }
            }
        );

        chrome.runtime.onInstalled.addListener(function(details) {
            if (details.reason == "install" && !(Storage.GetItem("guideShow"))) {
                chrome.tabs.create({url: "options.html"});
            } else if (details.reason == "update") {
                var thisVersion = chrome.runtime.getManifest().version;
                //console.log("Updated from " + details.previousVersion + " to " + thisVersion + "!");
            }
        });

        chrome.tabs.onActivated.addListener(function(info) {
            if (!fetching) {
                var tab = chrome.tabs.get(info.tabId, function(tab) {
                    if (Stats.isWebURL(tab.url)) {
                        if (typeof (loc_map[info.tabId]) != 'undefined'){
                            Stats.focus(tab.url);
                        }
                    }
                });
            }
            chrome.tabs.get(info.tabId, function(tab) {
                if ((Utils.validateUrl(tab.url) || tab.url === chrome.extension.getURL("options.html")) && 
                    Options.GetItem("showIcon") == true) {
                    setBrowserActionIcon(tab, true);
                }
            });
        });
        
        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
            // console.log("onUpdated");
            if (changeInfo.status == 'complete') {
                  if (Stats.isWebURL(tab.url)) {
                    loc_map[tabId] = tab.url;
                    fetching = true;
                    fetch(tab);
                }
            }
            if ((Utils.validateUrl(tab.url) || tab.url === chrome.extension.getURL("options.html")) && 
                Options.GetItem("showIcon") == true && 
                changeInfo.status == "loading") {
                setBrowserActionIcon(tab, true);
            }
        });

        chrome.tabs.onReplaced.addListener(function(addedTabId, removedTabId){
            fetching = true;
            var tab = chrome.tabs.get(addedTabId, function(tab) {
                if (Stats.isWebURL(tab.url)) {
                    loc_map[addedTabId] = tab.url;
                    if (typeof (loc_map[removedTabId]) != 'undefined'){
                        delete loc_map[removedTabId];
                    }
                    fetch(tab);
                }
            });
        });

        chrome.tabs.onSelectionChanged.addListener(function(id, selectInfo) {
            if (!fetching) {
                chrome.tabs.get(id, function(tab) {
                    if (Stats.isWebURL(tab.url)) {
                        if (typeof (loc_map[id]) != 'undefined'){
                            Stats.focus(tab.url);
                        }
                    }
                });
            }
        });

        chrome.tabs.onRemoved.addListener(function(id, removeInfo) {
            if (typeof (loc_map[id]) != 'undefined'){
                delete loc_map[id];
            }
        });

        function fetch(tab) {
            // console.log("fetch");
            var port = chrome.tabs.connect(tab.id, { name: "ref" });
            // console.log("posting message");
            port.postMessage({ message: "bg:get-ref" });
            port.onMessage.addListener(function(msg) {
                if (msg.message == "content:get-ref") {
                    Stats.loc(msg.url, msg.referrer);
                    fetching = false;
                    // console.log(msg);
                }
            });
        }
    };

    init();
//};