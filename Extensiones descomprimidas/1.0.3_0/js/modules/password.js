var Password = (function() {
	var check = function(password) {
        return password && Utils.hashCode(password) == Options.GetItem("password");
	};

	return {
		check: check
	};
})();