var SiteVerifier = (function() {
    var isTrustedSite = function(url) {
    	var domainInfo = Utils.extractDomain(url);
        var domain = domainInfo.domain;
        var url = Utils.cleanTheUrl(domainInfo.url); 
        var rootDomain = domainInfo.rootDomain;
        var isSubDomain = domainInfo.isSubDomain;
        var trusted_sites = Storage.GetItem("trusted_sites");
        var blockedSites = Storage.GetItem("blocked_sites");

        var isTrusted = false;
        if (trusted_sites.indexOf(url) !== -1) {
            isTrusted = true;
        }

        if (isSubDomain) {
            if (trusted_sites.indexOf(domain) !== -1 || (trusted_sites.indexOf(rootDomain) !== -1 && blockedSites.indexOf(domain) == -1)) {
                isTrusted = true;
            }
        } else if (!isSubDomain && trusted_sites.indexOf(domain) !== -1) {
            isTrusted = true;
        }

        if (bypassedDomain.indexOf(domain) !== -1) {
            isTrusted = true;
        }
        // console.log(isTrusted);
        return isTrusted;
    };

    var isBlockedSite = function(url) {
    	var domainInfo = Utils.extractDomain(url);
        var domain = domainInfo.domain;
        var url = Utils.cleanTheUrl(domainInfo.url); 
        var rootDomain = domainInfo.rootDomain;
        var isSubDomain = domainInfo.isSubDomain;
        var trusted_sites = Storage.GetItem("trusted_sites");
        var blockedSites = Storage.GetItem("blocked_sites");
        var blockedDetails = { isBlocked: false };

        if (blockedSites.indexOf(url) !== -1) {
            blockedDetails.isBlocked = true;
            blockedDetails.reason = History.BLOCK_REASONS.URL; // 0 for url, 1 for domain, 2 for keyword
        } else {
            if (isSubDomain) {
                if (blockedSites.indexOf(domain) !== -1) {
                    blockedDetails.isBlocked = true;
                    blockedDetails.reason = History.BLOCK_REASONS.DOMAIN; // 0 for url, 1 for domain, 2 for keyword
                } else if (trusted_sites.indexOf(domain) == -1 && blockedSites.indexOf(rootDomain) !== -1) {
                    blockedDetails.isBlocked = true;
                    blockedDetails.reason = History.BLOCK_REASONS.DOMAIN; // 0 for url, 1 for domain, 2 for keyword
                }   
            } else {
                if (blockedSites.indexOf(domain) !== -1) {
                    blockedDetails.isBlocked = true;
                    blockedDetails.reason = History.BLOCK_REASONS.DOMAIN; // 0 for url, 1 for domain, 2 for keyword
                }
            }
        }
        return blockedDetails;
    };    

    return {
    	isTrustedSite: isTrustedSite,
    	isBlockedSite: isBlockedSite
    };
})();