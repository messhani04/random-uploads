var cr = chrome.runtime;
var wotFilter = {
    trusted: false,
    blocked: false,
    bypassed: false,
    options: {},
    blocked_keywords: [],

    BUTTONS_TEXT: {
        GO_BACK: "Go Back",
        BYPASS: "Allow Once",
        TRUST_SITE: "Always Trust"
    },

    init: function() {
        // console.log("init");
        if (this.trusted) { return; }
        this.addBgListeners();
        this.getOptions(this.onStart);
    },

    getOptions: function(callback) {
        chrome.runtime.sendMessage({
            message: "options"
        }, function(options) {
            this.options = options;
            this.isTrustedSite(function() { 
                callback.bind(this)();  // onStart
            });
        }.bind(this));
    },

    onStart: function(options) {
        // console.log(this);
        if (!this.options["active"]) { return; }

        // console.log("onStart");

        $(function() {
            if (wotFilter.trusted) { return; }
            cr.sendMessage({
                message: "getBlockedKeywords"
            }, function(blocked_keywords) {
                wotFilter.blocked_keywords = blocked_keywords;
                wotFilter.searchBlockedKeywords();
            });
        });
    },

    clearHeadElement: function() { $("head").empty(); },
    clearBodyElement: function() { $("body").empty; },

    addBgListeners: function() {
        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                // console.log(request);
                var blockReason = request.blockReason;
                var isPopulateBlockPopup = request.populateBlockPopup;
                var domain = request.domain;
                var url = request.url;
                if (isPopulateBlockPopup == true) {
                    if (blockReason === 0) {
                        var blockedHtml = wotFilter.getBlockedTitleHtml(url, "is a blocked URL");
                        wotFilter.blockInterface(blockedHtml, url, domain, true);
                    }
                    else if (blockReason === 1) {
                        var blockedHtml = wotFilter.getBlockedTitleHtml(domain, "is a blocked site");
                        wotFilter.blockInterface(blockedHtml, url, domain, true);
                    }
                }
            }
        );
    },

    isTrustedSite: function(callback) {
        chrome.runtime.sendMessage({ 
            message: "isTrustedSite"
        }, function(response) {
            this.trusted = response.result;
            callback.bind(this)(); // onStart running here
        }.bind(this));
    },

    getBlockedTitleHtml: function(url, reason) {
        return "'<span class='momane_name' title='" + url + "'>" + url + "'</span> " + reason;
    },

    reportKeywordBlocking: function(keyword) {
        cr.sendMessage({
            message: "reportKeywordBlocking", 
            keyword: keyword
        });
    },

    sendBypassMessage: function(url, domain) {
        cr.sendMessage({ 
            message: "bypass",
            domain: domain,
            url: url
        });
    },

    sendAddTrustMessage: function(url, domain) {
        cr.sendMessage({ 
            message: "addTrust",
            domain: domain,
            url: url
        });
    },

    blockInterface: function(reason, url, domain, isBlockedFromBg) {
        window.stop();
        url = url || window.location.href;
        domain = domain || window.location.host;
        this.blocked = true;
        // console.log(url);
        // console.log(domain);
        var bypass = this.options["enable_bypass"]
        var passwordEnabled = this.options["password_active"];
        if (!isBlockedFromBg) {
            this.clearBodyElement();
            this.clearHeadElement();
        }

        var html_bypass = '\
            <div class="modal-controls">\
                <button class="button-back" id="momane_btn-back" style="border-radius: 20px 5px 5px 20px; cursor: pointer;">Go Back</button>\
                <button class="button-ok" id="momane_btn-bypass" style="background: #F5BE02;; cursor: pointer;">' + this.BUTTONS_TEXT.BYPASS + '</button>\
                <button class="button-trust" id="momane_trust" style= "border-radius: 5px 20px 20px 5px; cursor: pointer;">' + this.BUTTONS_TEXT.TRUST_SITE + '</button>\
            </div>';

        document.body.innerHTML = '\
            <div class="wot-options-body">\
                <div class="modal " id="modal-block" style="display:block">\
                    <div class="modal-dialog">\
                        <div class="momane_blockReason"></div>\
                        <div class="modal-title red-donut">\
                            &nbsp;\
                        </div>\
                        ' + html_bypass + '\
                    </div>\
                </div>\
            </div>';

        if (!bypass) { this.hideBypassButton(); }

        if (this.options["show_block_reason"]) {
            $(".momane_blockReason").html(reason).show();
        }

        var passwordHtml = '\
            <div class="modal" id="modal-password">\
                <div class="modal-dialog">\
                    <div class="modal-title exclamation">\
                        Enter Password\
                    </div>\
                    <div class="passwords">\
                        <label for="password">Enter Password</label>\
                        <input class="wide" type="password" id="momane_password"/>\
                    </div>\
                    <div class="modal-controls">\
                        <button class="button-cancel" id="btn-cancel-modal-password" style="cursor: pointer;">Cancel</button>\
                        <button class="button-ok" id="btn-ok-modal-password" style="cursor: pointer;">OK</button>\
                    </div>\
                </div>\
            <div>';

        $("body").on("click", "#momane_btn-back", function() {
            window.history.back();
        });

        $("body").on("click", "#momane_trust", function() {
            if (passwordEnabled) {
                checkPasswordAndShow(function(){
                    this.sendAddTrustMessage(url, domain);
                }.bind(this));
            } else {
                this.sendAddTrustMessage(url, domain);
            }
        }.bind(this));
        
        $("body").on("click", "#momane_btn-bypass", function() {
            if (passwordEnabled) {
                checkPasswordAndShow(function(){
                    this.sendBypassMessage(url, domain);
                }.bind(this));
            } else {
                this.sendBypassMessage(url, domain);
            }
        }.bind(this));

        function checkPasswordAndShow(callback) {
            // console.log(callback);
            if (!$("#modal-password").size()) {
                $(".wot-options-body").append(passwordHtml);
            }
            var passwordModel = $("#modal-password");
            var passwordIpt = $("#momane_password");
            passwordModel.show();
            passwordIpt.focus();
            $("#btn-cancel-modal-password").click(function() {
                passwordModel.hide();
            });

            passwordIpt.on("keypress", function(e) {
                if (e.keyCode === 13) {
                    checkPassword();
                }
            });

            $("#btn-ok-modal-password").click(checkPassword);

            function checkPassword() {
                var password = passwordIpt.val();
                cr.sendMessage({
                    message: 'checkPassword',
                    password: password
                }, function(response) {
                    if (response.passed) {
                        callback();
                    } else {
                        passwordIpt.shake({speed: 60});
                    }
                });
            }
        }
    },

    hideBypassButton: function() {
        $("#momane_btn-bypass").hide();
        $("#momane_trust").hide();
        $("#momane_btn-back").css("border-radius", "20px");
    },

    searchBlockedKeywords: function() {
        var domain = window.location.host.toLowerCase();
        if (domain.indexOf("google") !== -1) {
            setTimeout(function() {
                this.searchBlockedKeywords();
            }.bind(this), 3000);
        }

        // if (this.blocked) { 
        //     chrome.runtime.sendMessage({
        //         message: "reportKeywordBlocking",
        //         addToHis: false
        //     });
        //     return;
        // }

        if (this.trusted || this.blocked || this.bypassed) { return; }
        var head = document.getElementsByTagName('head')[0];
        var head_text = head ? head.innerText.toLowerCase() : '';
        var body = document.getElementsByTagName('body')[0];
        var body_text = body ? body.innerText.toLowerCase() : '';

        for (var i = 0; i < this.blocked_keywords.length; i++) {
            var kw = this.blocked_keywords[i].toLowerCase();
            var regexp = new RegExp('\\b' + kw + '\\b');
            var blockedHtml = wotFilter.getBlockedTitleHtml(kw, "is a blocked keyword");
            var urlWords = UrlWordsFinder.getWords(window.location);
            // console.log(urlWords);

            if (urlWords.indexOf(kw) !== -1) {
                wotFilter.blockInterface(blockedHtml);
                // console.log("reportKeywordBlocking 1");
                wotFilter.reportKeywordBlocking(kw);
                return;
            }
            if (head_text.match(regexp)) {
                wotFilter.blockInterface(blockedHtml);
                // console.log("reportKeywordBlocking 2");
                wotFilter.reportKeywordBlocking(kw);
                return;
            }
            if (body_text.indexOf(kw) >= 0) {
                if (body_text.match(regexp)) {
                    wotFilter.blockInterface(blockedHtml);
                    // console.log("reportKeywordBlocking 3");
                    wotFilter.reportKeywordBlocking(kw);
                    return;
                }
            }
        }
    }
};

wotFilter.init();


var UrlWordsFinder = new function() {
    var WORD_REG_EXP = /[A-Za-z]+/;

    var isQueryStringOn = false;
    var isHostOn = false;
    var isPathnameOn = false;

    this.init = function() {
        this.setQueryStringOn(true);
        this.setHostOn(true);
        this.setPathnameOn(true);
    };

    this.setQueryStringOn = function(value) { isQueryStringOn = value; };
    this.setHostOn = function(value) { isHostOn = value; };
    this.setPathnameOn = function(value) { isPathnameOn = value; };

    this.getWords = function(windowLocationObj) {
        if (!windowLocationObj) { return []; }

        var words = [];
        var queryStringWords = [];
        var hostWords = [];
        var pathnameWords = [];

        if (isQueryStringOn) {
            var queryString = this.getQueryString(windowLocationObj);
            var queryStringParams = this.getQueryStringParams(queryString);
            this.removeGoogleOqParam(queryStringParams);
            queryStringWords = this.extractQueryStringWords(queryStringParams);
        }

        if (isHostOn) { 
            hostWords = this.extractWordsFromHost(windowLocationObj);
        }

        if (isPathnameOn) {
            pathnameWords = this.extractWordsFromPathname(windowLocationObj);
        }

        words = words.concat(queryStringWords);
        words = words.concat(hostWords);
        words = words.concat(pathnameWords);

        words = this.removeDuplicates(words);

        return words;
    };

    this.removeDuplicates = function(words) {
        var words = words.filter(function(item, pos) {
            return words.indexOf(item) == pos;
        });

        return words;
    };

    this.getQueryString = function(windowLocationObj) {
        if (windowLocationObj.hasOwnProperty("search")) {
            return windowLocationObj.search;
        }
        var url = windowLocationObj.href;
        return url.indexOf("?") !== -1 ? url.split("?")[1] : null;
    };

    this.getQueryStringParams = function(queryString) {
        return queryString ? queryString.split("&") : null;
    };

    this.extractQueryStringWords = function(queryStringParams) {
        if (!queryStringParams) { return []; }
        var words = {
            params: [],
            values: []
        };

        var allWords = [];

        for (var i in queryStringParams) {
            var param = decodeURI(queryStringParams[i]).split("=");
            words.params.push(param[0].toLowerCase());
            words.values.push(param[1].toLowerCase());
        }

        for (var i in words.params) {
            var param = words.params[i];
            allWords = allWords.concat(this.extractWords(param));
        }

        for (var i in words.values) {
            var param = words.values[i];
            allWords = allWords.concat(this.extractWords(param));
        }

        return allWords;
    };

    this.removeGoogleOqParam = function(queryStringParams) {
        for (var i in queryStringParams) {
            if (queryStringParams[i].indexOf("oq=") === 0 ||
                queryStringParams[i].indexOf("pq=") === 0) {
                // console.log("found");
                queryStringParams.splice(i, 1);
            }
        }
    };    

    this.extractWords = function(words) {
        var results = [];
        var safeBreak = 0;
        do {
            if (safeBreak === 100) { return; }
            safeBreak++;

            var extractedWord = words.match(WORD_REG_EXP);
            if (!extractedWord) { break; }
            extractedWord = extractedWord.toString();
            var index = words.indexOf(extractedWord);
            index += extractedWord.length;
            results.push(extractedWord);
            words = words.substr(index);
        } while (true)

        return results;
    };

    this.extractWordsFromHost = function(windowLocationObj) {
        var host = windowLocationObj.host.split(".");
        host.splice(host.length-1, 1);

        return host;
    };

    this.extractWordsFromPathname = function(windowLocationObj) {
        var pathnameWords = windowLocationObj.pathname.split("/");
        var pathnameWords = pathnameWords.filter(function(item, pos) {
            return item !== "";
        });             
        return pathnameWords;
    };

    this.init();
};