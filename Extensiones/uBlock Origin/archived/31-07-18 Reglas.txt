no-large-media: behind-the-scene false
no-strict-blocking: ads.ad-center.com true
no-strict-blocking: ads.reawaken.men true
no-strict-blocking: play.leadzupc.com true
no-strict-blocking: www.mgid.com true
behind-the-scene * * noop
behind-the-scene * 1p-script noop
behind-the-scene * 3p noop
behind-the-scene * 3p-frame noop
behind-the-scene * 3p-script noop
behind-the-scene * image noop
behind-the-scene * inline-script noop